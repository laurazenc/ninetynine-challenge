# ninetine-challenge

## **How to run it**

### client

```
    cd client
    yarn install
    yarn start
```

### server

```
    cd server
    yarn install
    yarn start
```

## **Solution approach**

    I've decided to build a GraphQL Backend-For-Frontend (BFF)implementation to prove how benefitial this approach is for Frontend development.

    In this case the API is wrapped in the GraphQL Server and defines the model, then, the GraphQL client could call this GraphQL API and request only the needed fields for that view which helps saving data from each request.

### **Wrapping the API**

    The given ninetynine's api endpoints returns a list of companies and the detail of a company. And here is how I've modeled those into GraphQL.

```
type Company {
  id: Int!
  name: String!
  ric: String!
  sharePrice: Float!
  description: String
  country: String
}

type Error {
  path: String
  message: String
}

type getCompaniesResponse {
  data: [Company]!
  errors: [Error!]
}

type getCompanyDetailResponse {
  data: Company
  errors: [Error!]
}

type Query {
  getCompanies: getCompaniesResponse!
  getCompanyDetail(id: Int!): getCompanyDetailResponse!
}
```

### **Misc decisions**

    - Since this is for test porpuse, there is an **.env** file in the server which won`t be in a real project in production environment

    - The 'realtime' graph which shows the current price of a company uses the **D3** library

    - There is a **layout HOC** to define the view's structure.

    - For the CSS part I've used **styled-components** which, among other nice features, allows to wrap the whole app within a theme (using variables for colors, font sizes, common margin values...) and be able to define components sharing these.

### **Things to improve**

    I haven't use anything for managing states along the app, but it could be improve using the GraphQL cache in the client. There is also some new features from React v16 such as the context API, hooks or refs which as far as I know allows to share states within the app as well, but I've not dig enought into it to feel comfortable to use it in this challenge.

    Define a better error handling system

    Include tests for both the server and the client side

### **Conclusions**

    I had never made anything displaying data and it's been fun learning D3 to draw information into a linear graph.
