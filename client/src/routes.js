import React from "react";
import { Switch, BrowserRouter, Route } from "react-router-dom";

import Home from "./pages/Home";
import CompanyDetail from "./pages/CompayDetail";
import NotFound from "./pages/NotFound";

export const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/company/:id" component={CompanyDetail} />
      <Route component={NotFound} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
