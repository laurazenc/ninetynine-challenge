import React, { Component } from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";

import withFullScreenLayout from "../../layouts/FullScreenLayout";
import HomeView from "./HomeView";

const GETCOMPANIES_QUERY = gql`
  {
    getCompanies {
      data {
        id
        name
        ric
        sharePrice
      }
      errors {
        path
        message
      }
    }
  }
`;

class Home extends Component {
  render() {
    return (
      <Query query={GETCOMPANIES_QUERY}>
        {({ data, loading }) => {
          return <HomeView data={data} loading={loading} />;
        }}
      </Query>
    );
  }
}

export default withFullScreenLayout(Home);
