import React, { Component } from "react";
import styled from "styled-components";
import { formatPrice } from "./../../utils/helpers";

const Container = styled.div`
  width: inherit;
  height: inherit;
`;

const CompaniesList = styled.div`
  display: grid;
  justify-content: center;
  grid-template-columns: repeat(auto-fill, minmax(100px, 250px));
  grid-gap: 1rem;
  flex-direction: row;
  box-sizing: border-box;
`;

const CompanyCard = styled.a`
  position: relative;
  background-color: #262e47;
  border-radius: 6px;
  box-shadow: 1px 1px 10px 0px rgba(0, 0, 0, 0.3);
  height: 100px;
  color: white;
  padding: 10px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border: 1px solid transparent;
  text-decoration: none;

  &:hover {
    border-color: ${props => props.theme.blueColor};
    cursor: pointer;
  }
`;

const Name = styled.h4`
  margin-bottom: 8px;
`;
const Ric = styled.h6`
  margin-top: 8px;
  color: ${props => props.theme.purpleColor};
`;

const CurrentPrice = styled.div`
  color: ${props => props.theme.greenColor};
`;

export default class HomeView extends Component {
  render() {
    const { data, loading } = this.props;
    return (
      <Container>
        {loading && <div>Loading...</div>}
        {!loading && data && data.getCompanies.data.length > 0 && (
          <CompaniesList>
            {data.getCompanies.data.map(company => {
              return (
                <CompanyCard href={`/company/${company.id}`} key={company.id}>
                  <div>
                    <Name>{company.name}</Name>
                    <Ric>{company.ric}</Ric>
                  </div>
                  <CurrentPrice>{formatPrice(company.sharePrice)}</CurrentPrice>
                </CompanyCard>
              );
            })}
          </CompaniesList>
        )}
      </Container>
    );
  }
}
