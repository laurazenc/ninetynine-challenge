import React from "react";
import styled from "styled-components";
import { theme } from "./../../../theme";

import * as scale from "d3-scale";
import * as shape from "d3-shape";
import * as format from "d3-format";
import { scaleTime, scaleLinear } from "d3-scale";
import { min, max } from "d3-array";
import { mouse, select } from "d3-selection";
import { bisector, extent } from "d3-array";

const d3 = {
  scale,
  shape,
  format,
  mouse,
  bisector,
  select,
  extent,
  min,
  max
};

var margin = { top: 20, right: 20, bottom: 70, left: 40 };
const height = 400 - margin.top - margin.bottom;
const width = 800 - margin.left - margin.right;

let scaleX, scaleY, line, svg, svgContent;

const Container = styled.div`
  position: relative;
  width: inherit;
  display: flex;
  justify-content: center;
  align-items: center;

  svg {
    .tick {
      line {
        stroke: "transparent";
      }
      text {
        fill: ${props => props.theme.blueColor};
        transform: translateX(10);
      }
    }
    .dot {
      font-size: 10px;
    }
  }
`;

class Graph extends React.Component {
  state = {
    dataInfo: [],
    activePoint: null
  };
  componentWillMount() {
    const { data } = this.props;
    this.setState(prevState => ({
      dataInfo: [...prevState.dataInfo, { x: new Date().getTime(), y: data }],
      activePoint: null
    }));
    if (
      this.state.dataInfo.length > 1 &&
      this.state.dataInfo[this.state.dataInfo.length - 2].y !== data
    ) {
      this.getEqValue();
    }
    this.setVariables();
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { dataInfo } = nextState;
    if (dataInfo[dataInfo.length - 1].y !== nextProps.data) {
      this.setState(prevState => ({
        dataInfo: [
          ...prevState.dataInfo,
          { x: new Date().getTime(), y: nextProps.data }
        ],
        activePoint: null
      }));
      if (
        dataInfo.length > 1 &&
        dataInfo[dataInfo.length - 2].y !== nextProps.data
      ) {
        this.getEqValue();
      }
      this.setVariables();
      return true;
    } else {
      return false;
    }
  }

  getEqValue = () => {
    const { dataInfo } = this.state;
    const value =
      dataInfo.length > 1
        ? parseFloat(
            (100 *
              (dataInfo[dataInfo.length - 1].y -
                dataInfo[dataInfo.length - 2].y)) /
              ((dataInfo[dataInfo.length - 1].y +
                dataInfo[dataInfo.length - 2].y) /
                2)
          ).toFixed(2)
        : 100;

    this.props.valueChanged(value);
  };

  componentDidMount() {
    this.setVariables();
  }

  setVariables = () => {
    const { dataInfo } = this.state;
    const data = dataInfo;
    scaleX = scaleTime()
      .domain(d3.extent(data, d => d.x))
      .range([0, width]);

    scaleY = scaleLinear()
      .domain(d3.extent(data, d => d.y))
      .range([height, 0]);

    line = d3.shape
      .line()
      .x(d => scaleX(d.x))
      .y(d => scaleY(d.y))
      .curve(d3.shape.curveLinear)(data);

    svg = d3.select(".linechart");
    svgContent = d3.select(".svg-content");

    svgContent.attr("transform", `translate(${margin.top}, ${margin.left})`);

    const lineAndDots = d3.select(".d3-dots");
    const lineAndText = d3.select(".d3-texts");

    lineAndText.selectAll(".dot").remove();
    lineAndText
      .selectAll(".dot")
      .data(data)
      .enter()
      .append("text")
      .attr("class", "dot")
      .attr("stroke", "white")
      .attr("x", d => scaleX(d.x) - 10)
      .attr("y", d => scaleY(d.y) - 10)
      .text(d => d.y);

    lineAndDots.selectAll(".data-circle").remove();
    lineAndDots
      .selectAll(".data-circle")
      .data(data)
      .enter()
      .append("circle")
      .attr("class", "data-circle")
      .attr("r", 5)
      .attr("stroke", theme.purpleColor)
      .attr("fill", theme.backgroundColor)
      .attr("cx", d => scaleX(d.x))
      .attr("cy", d => scaleY(d.y));
  };
  render() {
    return (
      <Container className="container">
        <svg
          {...{
            width: width + margin.left + margin.right,
            height: height + margin.top + margin.bottom + 100
          }}
          className={"linechart"}
        >
          <g className={"svg-content"}>
            <defs>
              <linearGradient id="fill" x1="50%" y1="0%" x2="50%" y2="100%">
                <stop
                  stopColor={theme.blueColor}
                  stopOpacity=".2"
                  offset="0%"
                />
                <stop
                  stopColor={theme.purpleColor}
                  stopOpacity=".05"
                  offset="100%"
                />
              </linearGradient>
            </defs>
            <path
              d={line}
              fill="transparent"
              stroke={theme.purpleColor}
              strokeWidth="4"
            />
            <path
              d={`${line} L ${width} ${height} L 0 ${height}`}
              fill="url(#fill)"
            />
            <g className="d3-dots" />
            <g className="d3-texts" />
          </g>
        </svg>
      </Container>
    );
  }
}

export default Graph;
