import React from "react";
import { Query } from "react-apollo";
import styled from "styled-components";

import Graph from "./Graph";
import { COMPANYDETAILS_QUERY } from "./../index";

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: inherit;
`;

class SharePriceGraph extends React.Component {
  handleValueChange = value => {
    this.props.currentValue(value);
  };
  render() {
    const { id, sharePrice } = this.props;
    return (
      <Query
        query={COMPANYDETAILS_QUERY}
        variables={{ id }}
        pollInterval={7000}
      >
        {({ data, loading }) => {
          if (loading)
            return (
              <Container>
                <h5>Loading...</h5>
              </Container>
            );
          if (data && data.getCompanyDetail) {
            return (
              <Container>
                <Graph
                  data={data.getCompanyDetail.data.sharePrice || sharePrice}
                  valueChanged={this.handleValueChange}
                />
              </Container>
            );
          }
          return sharePrice;
        }}
      </Query>
    );
  }
}

export default SharePriceGraph;
