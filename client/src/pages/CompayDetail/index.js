import React, { Component } from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import withFullScreenLayout from "../../layouts/FullScreenLayout";

import CompanyDetailView from "./CompanyDetailView";

export const COMPANYDETAILS_QUERY = gql`
  query getCompanyDetail($id: Int!) {
    getCompanyDetail(id: $id) {
      data {
        id
        name
        description
        country
        ric
        sharePrice
      }
      errors {
        path
        message
      }
    }
  }
`;

class CompanyDetail extends Component {
  render() {
    const {
      match: {
        params: { id }
      }
    } = this.props;
    return (
      <Query query={COMPANYDETAILS_QUERY} variables={{ id: parseInt(id) }}>
        {({ data, loading, error }) => {
          return (
            <CompanyDetailView
              id={parseInt(id)}
              data={data}
              error={error}
              loading={loading}
            />
          );
        }}
      </Query>
    );
  }
}

export default withFullScreenLayout(CompanyDetail);
