import React, { Component } from "react";
import styled from "styled-components";

import SharePriceGraph from "./components/SharePriceGraph";

const Container = styled.div`
  width: inherit;
  height: inherit;
`;

const Info = styled.div`
  display: flex;
  flex-direction: row;
  margin-left: 2rem;

  .Info-data {
    margin-left: 16px;
  }
`;

const Title = styled.h3`
  color: white;
  margin: 0 0 8px;
`;

const Description = styled.h5`
  color: white;
  margin: 0 0 4px;
`;

const Ric = styled.div`
  border-radius: 50%;
  width: 70px;
  height: 70px;
  background-color: ${props => props.theme.purpleColor};
  display: flex;
  align-items: center;
  justify-content: center;

  span {
    color: white;
    font-weight: bold;
  }
`;

const Price = styled.h6`
  color: ${props => (props.isPositive ? props.theme.greenColor : "red")};
  margin: 0;
`;

class CompanyDetailView extends Component {
  state = {
    currentValue: 0
  };

  handleCurrentValue = value => {
    this.setState({ currentValue: value });
  };

  currentValueSimbol = () => {
    const { currentValue } = this.state;
    return currentValue >= 0;
  };

  render() {
    const { data, loading, error, id } = this.props;
    const { currentValue } = this.state;
    return (
      <Container>
        {loading && <div>Loading...</div>}
        {!loading && data && data.getCompanyDetail.data && (
          <>
            <Info>
              <Ric>
                <span>{data.getCompanyDetail.data.ric}</span>
              </Ric>
              <div className="Info-data">
                <Title>{data.getCompanyDetail.data.name}</Title>
                <Description>
                  {data.getCompanyDetail.data.description}
                </Description>
                <Price isPositive={this.currentValueSimbol()}>
                  {this.currentValueSimbol() ? "+" : ""}
                  {currentValue}%
                </Price>
              </div>
            </Info>
            <SharePriceGraph
              id={id}
              currentValue={this.handleCurrentValue}
              sharePrice={data.getCompanyDetail.data.sharePrice}
              data={data}
            />
          </>
        )}
      </Container>
    );
  }
}

export default CompanyDetailView;
