import React from "react";
import ReactDOM from "react-dom";
import { ApolloProvider } from "react-apollo";

import { ThemeProvider, createGlobalStyle } from "styled-components";

import Routes from "./routes";

import * as serviceWorker from "./serviceWorker";
import { client } from "./apollo";
import { theme } from "./theme";

const GlobalStyle = createGlobalStyle`
    html {
      box-sizing: border-box;
      background-color: ${props => props.theme.backgroundColor};
    }
    *, *:before, *:after {
      box-sizing: inherit;
    }
    body {
      @import url("https://fonts.googleapis.com/css?family=Roboto:300,400,500,700");
      font-family: "Roboto", sans-serif;
      font-size: 14px;
      margin: 0;
      padding: 0;
    }
  `;

ReactDOM.render(
  <ApolloProvider client={client}>
    <ThemeProvider theme={theme}>
      <React.Fragment>
        <Routes />
        <GlobalStyle />
      </React.Fragment>
    </ThemeProvider>
  </ApolloProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
