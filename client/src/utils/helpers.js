export const formatPrice = sharePrice => {
  return `${parseFloat(sharePrice).toFixed(2)}€`;
};
