export const theme = {
  textColor: "#242b42",
  greenColor: "#37E8D8",
  blueColor: "#458CF8",
  purpleColor: "#A378F2",
  backgroundColor: "#242B42",
  shadowColor: "#151927",
  boxShadowBase: "0 2px 4px rgba(0,0,0,0.1)"
};
