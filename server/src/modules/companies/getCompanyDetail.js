import axios from 'axios';
import { formatErrors, handleErrors } from '../../utils/helpers';

require('dotenv').config();

const companyDetailRequest = async id => axios.get(`${process.env.API_BASE_URL}companies/${id}`);

export const getCompanyDetail = async (_, { id }) => {
  try {
    const company = await companyDetailRequest(id);

    return { data: company.data };
  } catch (e) {
    const { path, message } = formatErrors(e)[0];
    return handleErrors(path, message);
  }
};
