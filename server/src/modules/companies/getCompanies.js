import axios from 'axios';
import { formatErrors, handleErrors } from '../../utils/helpers';

require('dotenv').config();

export const getCompanies = async () => {
  try {
    const companies = await axios.get(`${process.env.API_BASE_URL}companies`);
    return { data: companies.data };
  } catch (e) {
    const { path, message } = formatErrors(e)[0];
    return handleErrors(path, message);
  }
};
