import { getCompanies } from './getCompanies';
import { getCompanyDetail } from './getCompanyDetail';

export const resolvers = {
  Query: {
    getCompanies,
    getCompanyDetail,
  },
};
