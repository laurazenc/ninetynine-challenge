import { GraphQLServer } from 'graphql-yoga';
import { genSchema } from './schemas';
import { pubsub } from './utils/pubsub';

require('dotenv').config();

const schema = genSchema();

const server = new GraphQLServer({
  schema,
  context: { pubsub },
});

server.start(
  {
    port: process.env.PORT || process.env.SERVER_PORT,
    endpoint: '/',
    cors: {
      credentials: true,
      origin: process.env.FRONTEND_URL,
    },
  },
  ({ port }) => {
    console.log(`[⚙️ ] Server is up and running at http://localhost:${port}`);
  },
);
