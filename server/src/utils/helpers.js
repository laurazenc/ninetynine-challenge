export const handleErrors = (path, message) => ({
  errors: [
    {
      path,
      message,
    },
  ],
});

export const formatErrors = (err) => {
  const errors = [];
  if (err && err.inner) {
    if (!err.inner.length) {
      errors.push({
        path: err.path,
        message: err.message,
      });
    } else {
      err.inner.forEach((e) => {
        errors.push({
          path: e.path,
          message: e.message,
        });
      });
    }
  }

  return errors;
};
